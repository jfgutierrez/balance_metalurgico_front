import React from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import CargaDatos from './components/main/CargaDatos.js';
import Configuracion from './components/configuracion/Configuracion.js';

function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={CargaDatos}  />
        <Route path="/cargardatos" component={CargaDatos}/>
        <Route path="configuracion" component={Configuracion}/>
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
