import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Exportar from './ExportarDatos.js';
import Configuracion from '../configuracion/Configuracion.js';

import Cookies from 'js-cookie';


function TabPanel(props) {
    const{ children, value, index } =props
    return(
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
        >
            {value === index && (
              <Box p={1}>
                <Typography component= "span">{children}</Typography>
              </Box>
            )}
        </div>
    );
}
TabPanel.propTypes ={
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};

function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const useStyles = makeStyles({
    root:{
        flexGrow: 1,
    },
});
function CargaDatos() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const handleChange = (e, newValue) =>{
        setValue(newValue);
    };
    var username = Cookies.get('ClienteId');
    console.log(username);
    return (
        <div>
            <div className="container">
                <Paper className={classes.root}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor= "inherit"
                        centered={false}
                        >
                        <Tab label="Exportar Excel" {...a11yProps(0)} />
                        <Tab label="Configuraci&oacute;n" {...a11yProps(1)} />
                    </Tabs>
                </Paper>
            </div>
            <div>
                <TabPanel value={value} index={0}>
                    <div className="container">
                        <Exportar/>
                    </div>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <div className ="container">
                        <Configuracion/>
                    </div>
                </TabPanel>
            </div>
        </div>
    )
}

export default CargaDatos
