import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

import * as XLSX from 'xlsx';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as Icon from 'react-bootstrap-icons';

import './Balance.css';
import {Alert, AlertTitle} from '@material-ui/lab';

import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

import Cookies from 'js-cookie';
import swal from 'sweetalert';

//import {Spinner} from 'reactstrap'

const columns = [
  { id: 'id', label: 'ID', minWidth: 170, align:'center' },
  { id: 'patio', label: 'Patio', minWidth: 100,  align:'center' },
  {
    id: 'flujoRiego',
    label: 'Flujo Riego',
    minWidth: 170,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'flujoPLS',
    label: 'Flujo PLS',
    minWidth: 170,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'leyCu',
    label: 'Ley Cu g/l',
    minWidth: 170,
    align: 'center',
    format: (value) => value.toFixed(2),
  },
  {
      id:'tonelaje',
      label:'Tonelaje',
      minWidth: 170,
      align: 'center',
      format: (value) => value.toFixed(2),
  },
];

const columnsControl = [
  { id: 'id', label: 'ID', minWidth: 170, align:'center' },
  { id: 'variable', label: 'Variable', minWidth: 100,  align:'center' },
  {
    id: 'valor',
    label: 'Valor',
    minWidth: 170,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
    marginTop: 4
  },
  marginTable:{
    padding: 2
  },
});

export default function StickyHeadTable() {
  const userName = Cookies.get('ClienteId') !== "" ? Cookies.get('ClienteId') : "Importador";

  const url = "http://10.240.250.88:9095/api/exportar"; //Produccion
  //const url = "http://localhost:23284/api/exportar"; //Local development
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [items, setItems] = useState([]);
  const [startDate, setStartDate] = useState(new Date());
  const [pageContrl, setPageContrl] = React.useState(0);
  const [rowsPerPageContrl, setRowsPerPageContrl] = React.useState(5);
  const [alertConfig, setAlertConfig] = React.useState({
    opacity:0,
    severit:"success",
    message:"",
    title:""
  });
  const [blocking, setBlocking]= React.useState(false);

    const readExcel = async(file) => {
      setBlocking(true);
        const promise = new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(file);

        fileReader.onload = (e) => {
            const bufferArray = e.target.result;
            const wb = XLSX.read(bufferArray, { type: "buffer" });
            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];
            const data = XLSX.utils.sheet_to_json(ws);
            resolve(data);
            setBlocking(false);
        };

        fileReader.onerror = (error) => {
            reject(error);
            setBlocking(false);
        };
        });

        promise.then((d) => {
        setItems(d);
        console.log(d);
        setBlocking(false);
        getFecha(d[0].Fecha);
        });
    }

  var fecha;
  const getFecha = (dias) =>{
    fecha = new Date((dias - (25567 + 1)) * 86400 *1000);
    console.log(fecha.toLocaleDateString());
    setStartDate(fecha);
  }
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangePageContrl = (event, newPageContrl) => {
      setPageContrl(newPageContrl);
    };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(event.target.value);
    setPage(0);
  };
  const handleChangeRowsPerPageContrl = (event) => {
      setRowsPerPageContrl(event.target.value);
      setPageContrl(0);
    };

  const history = useHistory();
  const exportarDatosPost =async(items) =>{
    setBlocking(true);
    var fecha = startDate.toLocaleDateString();
    var param ={dataFront:items, fechaFront: fecha, userName: userName};
    console.log(param);
    if(items.length > 0){
      await axios.post(url+"/savedata",param).then((response) =>{
        console.log(response);
        history.push("/");
        setAlertConfig({opacity:1, severit:"success", message:"Datos guardados exitosamente!!", title:"Success"});
        setTimeout(()=> setAlertConfig({opacity:0, severit:"success", message:"Datos guardados exitosamente!!", title:"Success"}),3000);
        setBlocking(false);
      }).catch(error =>{
        console.log(error);
        setAlertConfig({opacity:1, severit:"error", message: error.message, title:"Error"});
        setTimeout(()=> setAlertConfig({opacity:0, severit:"error", message: error.message, title:"Error"}),3000);
        setBlocking(false);
      });
    }
    else{
      setAlertConfig({opacity:1, severit:"warning", message: "No hay datos", title:"Warning"});
      setTimeout(()=> setAlertConfig({opacity:0, severit:"warning", message: "No hay datos", title:"Warning"}),3000);
      setTimeout(() =>setBlocking(false),2000);
    }
  }

  const onClickShowAlertConfirm=()=>{
    swal({title:"Confirmar", 
          text:"¿Esta seguro(a) que desea exportar la información?", 
          icon:"info", 
          buttons:["No","Si"]
        }).then(response=>{
          if(response){
            exportarDatosPost(items);
          }
    })
  }
  return (
    <div>
      <BlockUi tag="div" blocking={blocking} message="Cargando...">
        <Paper className={classes.root}>
        <div className="containerMargen">
            <h1 style={{textAlign:"center"}}>Balance Metal&uacute;rgico - Sistema Producci&oacute;n</h1>
            <div className="row">
                <div className="col-md-5">
                    <input style={{marginLeft:"10px"}} className="fileMargen form-control"
                        type="file"
                        onChange={(e) => {
                        if(e.target.files.length > 0)
                        {
                            const file = e.target.files[0];
                            readExcel(file);
                        }
                        else{
                            alert("No se ha seleccionado ningun archivo");
                        }
                      }
                    }
                    />
                </div>
                <div className="col-md-2">
                  <DatePicker className="form-control" selected={startDate} 
                    onChange={(date) => setStartDate(date)} 
                    popperPlacement="top-end"/>
                </div>
                <div className="col-md-2">
                    <button className="btn btn-success btn-md" onClick={()=> onClickShowAlertConfirm()}>
                        Exportar Datos
                    </button>
                </div>
                <div className="col-md-2">
                  <span style={{textTransform:'uppercase'}}>{userName} </span>
                  <Icon.PersonCircle size={45}></Icon.PersonCircle>
                </div>
            </div>
        </div>
        <h6 style={{marginLeft:"10px", marginTop:"5px"}}>INFORMACI&Oacute;N DE PATIOS</h6>
        <TableContainer className={classes.container}>
            <Table aria-label="sticky table">
            <TableHead>
                <TableRow>
                {columns.map((column) => (
                    <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    >
                    <strong>{column.label}</strong>
                    </TableCell>
                ))}
                </TableRow>
            </TableHead>
            <TableBody>
                {
                items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((item) =>(
                    <TableRow hover role="checkbox" tabIndex={-1} key = {item.IDFila}>
                        <TableCell align ="center" >{item.IDFila != null ? item.IDFila : "N/A"}</TableCell>
                        <TableCell align ="center">{item.Patio != null ? item.Patio : "N/A"}</TableCell>
                        <TableCell align ="center">{item.FlujoRiego!= null ? item.FlujoRiego.toFixed(3):"N/A"}</TableCell>
                        <TableCell align ="center">{item.FlujoPLS != null ? item.FlujoPLS.toFixed(3):"N/A"}</TableCell>
                        <TableCell align ="center">{item.FlujoPLS != null ? item.LeyCu.toFixed(3):"N/A"}</TableCell>
                        <TableCell align ="center">{item.FlujoPLS != null ? item.Toneladas.toFixed(3):"N/A"}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
            </Table>
        </TableContainer>
        <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={items.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
        />
        <h6 style={{marginLeft:"10px"}}>PAR&Aacute;METROS DE CONTROL</h6>
        <div className="col-md-6">
        <TableContainer className={classes.container}>
            <Table aria-label="sticky table">
            <TableHead>
                <TableRow>
                {columnsControl.map((column) => (
                    <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    >
                    <strong>{column.label}</strong>
                    </TableCell>
                ))}
                </TableRow>
            </TableHead>
            <TableBody>
                {
                items.slice(pageContrl * rowsPerPageContrl, pageContrl * rowsPerPageContrl + rowsPerPageContrl).map((item) =>(
                    <TableRow hover role="checkbox" tabIndex={-1} key = {item.IDFila}>
                        <TableCell align ="center" >{item.IDFila}</TableCell>
                        <TableCell align ="center">{item.Concepto != null ? item.Concepto: "N/A"}</TableCell>
                        <TableCell align ="center">{item.Valor != null ? item.Valor.toFixed(3): "N/A"}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
            </Table>
        </TableContainer>
        <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={items.length}
            rowsPerPage={rowsPerPageContrl}
            page={pageContrl}
            onPageChange={handleChangePageContrl}
            onRowsPerPageChange={handleChangeRowsPerPageContrl}
        />
        </div>
        </Paper>
        </BlockUi>
        <div id="Alerts" className="row" >
            <div id="success" className={classes.root} style={{opacity: alertConfig.opacity, transition:"opacity 1s"}}>
                <Alert severity={alertConfig.severit}>
                    <AlertTitle>{alertConfig.title}</AlertTitle>
                    <strong>{alertConfig.message}</strong>
                </Alert>
            </div>
        </div>
    </div>
  );
}

