import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

import {makeStyles} from '@material-ui/core/styles';
import {Alert, AlertTitle} from '@material-ui/lab';

import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';

class Configuracion extends Component {
    constructor(props){
        super(props);
        //this.stateVarialeSelecionada={flujoRiego:0};
        this.handleChangecmbFlujoRiego = this.handleChangecmbFlujoRiego.bind(this);
        this.handleChangecmbFlujoPLS = this.handleChangecmbFlujoPLS.bind(this);
        this.handleChangecmbLeyCu = this.handleChangecmbLeyCu.bind(this);
        this.handleChangecmbTonelaje = this.handleChangecmbTonelaje.bind(this);
        this.handleChangetxt = this.handleChangetxt.bind(this);
        this.handleChangecmbTipoConfiguracion = this.handleChangecmbTipoConfiguracion.bind(this);
        this.getVariables();
        this.getConfiguracion();
        this.state = {
            IDFila:'',
            patio:'',
            flujoRiego:'',
            flujoPLS:'',
            leyCu:'',
            tonelaje:'',
            variable:[],
            configuracion:[],
            mensaje: '',
            opacitySuccess: 0,
            opacityWarning: 0,
            blocking: false,
            configEdit:[],
            tipoConfiguracion:-1
        }
        this.handleSuccess = this.handleSuccess.bind(this);
        this.handleWarning = this.handleWarning.bind(this);
    }    
    url = "http://10.240.250.88:9095/api/exportar"; //Produccion
    //url = "http://localhost:23284/api/exportar"; //Local development

    getVariables(){
        axios.get(this.url+"/getvariables")
        .then(response=>{
            this.setState({variable: response.data});
        })
        .catch(error=>{console.log(error)
        });
    }
    
    getConfiguracion(){
        axios.get(this.url +"/config").then(response=>{
            this.setState({configuracion: response.data});
        }).catch(error=>{
            console.log(error);
        });
    }
    handleChangetxt(event){
        this.setState({...this.state.datosConfiguracion, [event.target.name]:event.target.value})
    }
    handleChangecmbFlujoRiego(event){
        this.setState({flujoRiego: event.target.value});
    } 
    handleChangecmbFlujoPLS(event){
        this.setState({flujoPLS: event.target.value});
    } 
    handleChangecmbLeyCu(event){
        this.setState({leyCu: event.target.value});
    } 
    handleChangecmbTonelaje(event){
        this.setState({tonelaje: event.target.value});
    }

    handleChangecmbTipoConfiguracion(event){
        this.setState({tipoConfiguracion: event.target.value});
        if(event.target.value === "0"){
            document.getElementById("configuracionPatios").style.display = 'block';
            document.getElementById("configuracionControl").style.display = 'none';
        }
        else if(this.state.tipoConfiguracion === "1")
        {
            document.getElementById("configuracionPatios").style.display = 'none';
            document.getElementById("configuracionControl").style.display = 'block';
        }
        else{
            document.getElementById("configuracionPatios").style.display = 'none';
            document.getElementById("configuracionControl").style.display = 'none';
        }
    }
    handleSuccess(){
        document.getElementById("success").hidden = false;
        document.getElementById("warning").hidden = true;
        this.setState({opacitySuccess: 1}, () => {
            if(!this.timeout)
                clearTimeout(this.timeout);
            this.timeout = setTimeout(() => this.setState({opacitySuccess:0}),4000);
     })
    }
    handleWarning(){
        document.getElementById("success").hidden = true;
        document.getElementById("warning").hidden = false;
        this.setState({opacityWarning: 1}, () => {
            if(!this.timeout)
                clearTimeout(this.timeout);
            this.timeout = setTimeout(() => this.setState({opacityWarning:0}),4000);
     })
    }
    async onClickButtonSave(){
        this.state.blocking = true;
        var data = {
            IDFila:'',
            patio:'',
            flujoRiego:'',
            flujoPLS:'',
            leyCu:'',
            tonelaje:''
        }
        var isValid = true, mensaje="";
        console.log(this.state.blocking);

        if(this.state.IDFila !== "" && this.state.patio !== "" && this.state.flujoRiego !== "" 
            && this.state.flujoPLS !== "" &&this.state.leyCu !== "" && this.state.tonelaje !== ""){
            data.IDFila = parseInt(this.state.IDFila);
            data.patio = this.state.patio;
            data.flujoRiego = parseInt(this.state.flujoRiego);
            data.flujoPLS = parseInt(this.state.flujoPLS);
            data.leyCu = parseInt(this.state.leyCu);
            data.tonelaje = parseInt(this.state.tonelaje);
        }
        else{
            isValid = false;
            mensaje="Existen campos vacios, favor de revisar.";
            this.setState({mensaje: mensaje});
        }
        if(isValid === true){
            axios.post(this.url+"/saveconfig", data).then(response =>{
                this.setState({configuracion: response.data});
                this.limpiarCampos();
                setTimeout(() => this.state.blocking = false,2000);
                this.handleSuccess();
            }).catch(error =>{
                console.log(error);
            });
        }
        else{
            this.limpiarCampos();
            this.handleWarning();   
            setTimeout(() => this.state.blocking = false,4000);
        }
        
    }
    limpiarCampos(){
        document.getElementById("IDFila").value ="";
        document.getElementById("patio").value ="";
        document.getElementById("flujoRiego").value ="-1";
        document.getElementById("flujoPLS").value ="-1";
        document.getElementById("leyCu").value ="-1";
        document.getElementById("tonelaje").value ="-1";
    }

    onClickButtonEdit(config){
        this.setState({configEdit: config});
    }
    render() {
        const columns = [
            { id: 'id', label: 'Fila plantilla', minWidth: 170, align:'center', isHidden:false },
            { id: 'patio', label: 'Patio', minWidth: 100,  align:'center',isHidden:true },
            {
              id: 'flujoRiego',
              label: 'Flujo Riego',
              minWidth: 170,
              align: 'center',
              format: (value) => value.toLocaleString('en-US'),
            },
            {
              id: 'flujoPLS',
              label: 'Flujo PLS',
              minWidth: 170,
              align: 'center',
              format: (value) => value.toLocaleString('en-US'),
            },
            {
              id: 'leyCu',
              label: 'Ley Cu g/l',
              minWidth: 170,
              align: 'center',
              format: (value) => value.toFixed(2),
            },
            {
                id:'tonelaje',
                label:'Tonelaje',
                minWidth: 170,
                align: 'center',
                format: (value) => value.toFixed(2),
            },
            {
                id:'acciones',
                label:'Acción',
                minWidth: 170,
                align: 'center',
                format: (value) => value.toFixed(2),
            },
          ];

        const classes = makeStyles((theme)=>({
            root:{
                width: '100%',
                '& > * + *': {
                    marginTop: theme.spacing(2),
                  },
            },
        }));

        return (
            <div className="container" style={{backgroundColor:"white", borderRadius:"5px"}}>
                <div className="row">
                    <div className="col-md-3">
                        <label htmlFor="cmbTipoConfig">Tipo configuraci&oacute;n</label>
                        <select className="form-control" onChange={this.handleChangecmbTipoConfiguracion}>
                            <option value="-1">--Seleccionar--</option>
                            <option value="0">Configuración de Patios</option>
                            <option value="1">Configuración de control</option>
                        </select>
                    </div>
                </div>
                <BlockUi tag="div" blocking={this.state.blocking} message="Cargando...">
                    <div id="configuracionPatios" style={{display:'none'}}>
                        <div className="row">
                            <div className="col-md-3">
                                <label htmlFor="IDFila">N&uacute;mero de fila</label>
                                <input className="form-control" required type="text" name="IDFila" id="IDFila" placeholder="No. Fila" onChange={this.handleChangetxt}/>
                            </div>
                            <div className="col-md-3">
                                <label htmlFor="IDFila">Nombre de patio</label>
                                <input className="form-control" type="text" name="patio" id="patio" placeholder="Patio" onChange={this.handleChangetxt}/>
                            </div>
                            <div className="col-md-1">
                                <button className="btn btn-success" onClick={()=> this.onClickButtonSave()}>Agregar</button>
                            </div>
                            <div className="col-md-1">
                                <button className="btn btn-danger" onClick={()=> this.limpiarCampos()}>Cancelar</button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <label htmlFor="flujoRiego">Fujo de Riego</label>
                                <select value={this.state.flujoRiego} name="flujoRiego" id="flujoRiego"className="form-control"onChange={this.handleChangecmbFlujoRiego}>
                                    <option value="-1" defaultValue>--Seleccionar--</option>
                                    { this.state.variable.map((v)=>(
                                        <option className="form-control" key={v.id} value={v.id} >{v.descripcion}</option>
                                    ))
                                    }
                                </select>
                            </div>
                            <div className="col-md-3">
                                <label htmlFor="flujoPLS">Fujo PLS</label>
                                <select value={this.state.flujoPLS} name="flujoPLS" id="flujoPLS"className="form-control"onChange={this.handleChangecmbFlujoPLS}>
                                    <option value="-1"  defaultValue>--Seleccionar--</option>
                                    { this.state.variable.map((v)=>(
                                        <option className="form-control" key={v.id} value={v.id} >{v.descripcion}</option>
                                    ))
                                    }
                                </select>
                            </div>
                            <div className="col-md-3">
                                <label htmlFor="leyCu">Ley Cu</label>
                                <select value={this.state.leyCu} name="leyCu" id="leyCu"className="form-control"onChange={this.handleChangecmbLeyCu}>
                                    <option value="-1"  defaultValue>--Seleccionar--</option>
                                    { this.state.variable.map((v)=>(
                                        <option className="form-control" key={v.id} value={v.id} >{v.descripcion}</option>
                                    ))
                                    }
                                </select>
                            </div>
                            <div className="col-md-3">
                                <label htmlFor="tonelaje">Tonelaje</label>
                                <select value={this.state.tonelaje} name="tonelaje" id="tonelaje"className="form-control"onChange={this.handleChangecmbTonelaje}>
                                    <option value="-1"  defaultValue>--Seleccionar--</option>
                                    { this.state.variable.map((v)=>(
                                        <option className="form-control" key={v.id} value={v.id} >{v.descripcion}</option>
                                    ))
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <table style={{textAlign:"center", fontSize:"13px", borderRadius:"5px"}} className="table table-hover">
                                <thead>
                                    <tr>
                                    {columns.map((column) => (
                                        <th key={column.id}
                                            hidden ={column.isHidden}
                                            align={column.align}
                                            style={{ minWidth: column.minWidth }}
                                        >
                                        {column.label}
                                        </th>
                                    ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.configuracion.map((config)=>(
                                    
                                        <tr key={config.id}>
                                            <td>{config.idFilaPlantilla}</td>
                                            {this.state.variable.filter(v => v.id === config.flujoRiego_ID ||
                                                                        v.id === config.flujoPLS_ID ||
                                                                        v.id === config.leyCu_ID || 
                                                                        v.id === config.tonelaje_ID).map(found =>(
                                                                        <td key={found.id}>{found.descripcion}</td>
                                                                        ))}
                                            <td>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <button className="btn btn-warning btn-sm"onClick={()=> this.onClickButtonEdit(config)}>Editar</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        <div id="Alerts" className="row" >
                            <div id="success" className={classes.root} style={{opacity: this.state.opacitySuccess, transition:"opacity 1s"}}>
                                <Alert severity="success">
                                    <AlertTitle>Success</AlertTitle>
                                    <strong>Datos guardados con exitosamente!!</strong>
                                </Alert>
                            </div>
                            <div id="warning" className={classes.root} style={{opacity: this.state.opacityWarning, transition:"opacity 1s"}}>
                                <Alert severity="warning">
                                    <AlertTitle>Error</AlertTitle>
                                    <strong>{this.state.mensaje}</strong>
                                </Alert>
                            </div>
                        </div>
                    </div>
                    <div id="configuracionControl" style={{display:'none'}}>

                    </div>
                </BlockUi>
            </div>
        )
    }
}

export default Configuracion
